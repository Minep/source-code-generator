import unittest
from utils import parseStringFormat

class UtilTest(unittest.TestCase):
    def testParseStringFormate(self):
        args = parseStringFormat("/acda/cdfs/{package}/sacd{asdd}")
        self.assertDictEqual(args, {
            "package": "",
            "asdd": ""
        })

    def testParseStringFormat2(self):
        args = parseStringFormat("/acda/{package}{asdd}")
        self.assertDictEqual(args, {
            "package": "",
            "asdd": ""
        })

    def testParseStringFormat3(self):
        args = parseStringFormat("/acda/ccds/acdfs")
        self.assertEqual(len(args), 0)

if __name__ == '__main__':
    unittest.main()