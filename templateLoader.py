from pathFormatter import FormatterMananger
from typing import Set, Text
from structures import SourceTemplate
import os
import json
from pathlib import Path


class TemplateLoader:
    __templateNames: Set[Text]
    __baseDir: Text
    __configFileName: Text
    __formatterManager: FormatterMananger
    def __init__(self, formatterManager: FormatterMananger, templateScanFolder: Text, configFileName = "config.json") -> None:
        self.__baseDir = templateScanFolder
        self.__templateNames = set()
        self.__configFileName = configFileName
        self.__formatterManager = formatterManager
        for dir in os.scandir(templateScanFolder):
            if not dir.is_dir():
                continue
            configFile = Path(dir.path, self.__configFileName)
            if configFile.exists():
                self.__templateNames.add(dir.name)

    @property
    def getTemplates (self):
        return [x for x in self.__templateNames]

    def getTemplateDefinition(self, name) -> SourceTemplate:
        if not (name in self.__templateNames):
            raise ValueError("Unknown template: "+name)
        configFile = os.path.join(self.__baseDir, name, self.__configFileName)
        with open(configFile) as f:
            configuration = json.load(f)
            if type(configuration).__name__ != "dict":
                raise ValueError("Invalid configuration file format: Must be object")
            return SourceTemplate(
                    self.__formatterManager, 
                    os.path.join(self.__baseDir, name, "src"), 
                    configuration
                )
            