from enum import Enum
from pathFormatter import FormatterMananger
from typing import Callable, Dict, List, Text

from prompt_toolkit.completion.base import Completer
from prompt_toolkit.completion.fuzzy_completer import FuzzyWordCompleter
from structures import SourceTemplate
from jinja2.environment import Environment
from templateLoader import TemplateLoader
from generator import Generator
from prompt_toolkit.validation import Validator
from prompt_toolkit import prompt, print_formatted_text, HTML
from validators import PathValidator, IntValidator, FloatValidator
import os

class AppState(Enum):
    RESTART = 0,
    EXIT = 1,
    CONTINUE = 2

class SourceGeneratorApp:
    templates: TemplateLoader
    templateEnv: Environment
    validators: Dict[Text, Validator]
    allTemplates: List[Text]
    templateCompleter: Completer
    actionCompleter: Completer
    actions: Dict[str, Callable[[], AppState]]
    __formatterManager: FormatterMananger

    def __init__(self) -> None:
        folder = os.path.dirname(os.path.realpath(__file__))
        self.templateEnv = Environment(
            block_start_string="<%",
            block_end_string="%>",
            variable_start_string="{:",
            variable_end_string=":}"
        )
        self.__formatterManager = FormatterMananger()
        self.templates = TemplateLoader(
            self.__formatterManager, 
            os.path.join(folder, "templates")
        )
        self.allTemplates = self.templates.getTemplates
        self.templateCompleter = FuzzyWordCompleter(self.allTemplates)

        self.validators = {
            "str": None,
            "int": IntValidator(),
            "float": FloatValidator()
        }

        self.actions = {
            "list": self.displayTemplates,
            "start": self.startFlow,
            "help": self.printHelp,
            "exit": lambda: AppState.EXIT
        }

        self.actionCompleter = FuzzyWordCompleter(self.actions.keys())

    def printHelp(self) -> AppState:
        print((
            "\t list     List all available templates\n"
            "\t start    Start the generation flow\n"
            "\t exit     Exit the software\n"
            "\t help     Print this message\n"
        ))
        return AppState.CONTINUE
    
    def displayTemplates(self) -> AppState:
        print("           Templates           ")
        print("===============================")
        for tempName in self.allTemplates:
            print("   - {name}".format(name=tempName))
        print("===============================")
        return AppState.CONTINUE

    def startFlow(self) -> AppState:
        template = self.askForTemplate()
        if (template == None):
            return AppState.CONTINUE
        notNeedReview = False
        while not notNeedReview:
            self.askForConfig(template)
            notNeedReview = self.export(template)
        return AppState.CONTINUE

    def askForConfig(self, template: SourceTemplate):
        for cfg in template.configurables:
            value = ""
            hint = "{key} <- ".format(
                key=cfg.key,
                default=cfg.value
            )
            if cfg.type == 'str':
                value = prompt(hint, default=str(cfg.value))
            elif cfg.type == 'int':
                value = int(prompt(hint, validator=IntValidator(), default=str(cfg.value)))
            elif cfg.type == 'float':
                value = float(prompt(hint, validator=FloatValidator(), default=str(cfg.value)))
            else:
                continue
            template.setConfiguration(cfg.key, value)
    
    def askForTemplate(self) -> SourceTemplate:
        error = HTML("<ansired><b>Unable to find this template</b></ansired>")
        try:
            selectedTemplate = prompt("Select template: ", completer=self.templateCompleter, complete_while_typing=True)
            return self.templates.getTemplateDefinition(selectedTemplate)
        except ValueError:
            print_formatted_text(error)
            return None

    def export(self, template: SourceTemplate):
        result = prompt("Do you want to generate code using the above configurations? [y|n|r] (y=yes, n=no, r=redo configuration)").lower()
        if result == 'y':
            basePath = prompt("Where should I put the code? ", validator=PathValidator())
            name = prompt("What is the name of the project? ", default=template.name)
            print("Generating project, depend on the size of template, could be take a while ...")
            generator = Generator(self.templateEnv, template)
            generator.run(basePath, name)
            print("Done!")
        elif result == 'n':
            print("Aborting...")
        else:
            return False
        return True

    def dispatchAction (self):
        action = prompt("# ", completer=self.actionCompleter, complete_while_typing=True)
        handler = self.actions.get(action)
        if handler == None:
            print_formatted_text(HTML('<ansired>Unknown command: '+ action +'</ansired>'))
            return AppState.CONTINUE
        return handler()
    
    def __clear(self):
        os.system('cls' if os.name=='nt' else 'clear')

    def run(self):
        self.__clear()
        appState = AppState.CONTINUE
        while appState!=AppState.EXIT:
            print("      Source Code Generator         ")
            print("                       v 1.0.0.0    ")
            print("------------------------------------")
            print("Enter 'help' for help")
            print()
            while appState == AppState.CONTINUE:
                appState = self.dispatchAction()
            self.__clear()