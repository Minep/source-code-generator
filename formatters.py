from abc import ABC, abstractmethod
from typing import Any, Dict, Match, Text
from configurable import ConfigurableCollection
from utils import parseStringFormat
from os import path


class PathFormatter(ABC):
    _text: Text
    def __init__(self, path: Text) -> None:
        super().__init__()
        self._text = path

    @abstractmethod
    def getText(self, configCollection: ConfigurableCollection, processedMatch: Match) -> Text:
        return self._text

class RegexFormatter(PathFormatter):
    compiled: Text
    args: Dict[Text, Any]
    def __init__(self, path: Text) -> None:
        super().__init__(path)
        self.compiled, self.args = path, parseStringFormat(path)
    
    def getText(self, configCollection: ConfigurableCollection, processedMatch: Match) -> Text:
        for k in self.args.keys():
            if k.startswith("$"):
                self.args[k] = processedMatch.group(int(k[1:]))
            elif configCollection.hasConfig(k):
                self.args[k] = str(configCollection.getConfig(k).value)

        return self.compiled.format(**self.args)


class JavaPackageFormatter(PathFormatter):
    compiled: Text
    args: Dict[Text, Any]
    def __init__(self, path: Text) -> None:
        super().__init__(path)
        self.compiled, self.args = path, parseStringFormat(path)

    def getText(self, configCollection: ConfigurableCollection, processedMatch: Match) -> Text:
        for k in self.args.keys():
            if k.startswith("$"):
                self.args[k] = processedMatch.group(int(k[1:]))
            elif configCollection.hasConfig(k):
                self.args[k] = self.__packageToPath(str(configCollection.getConfig(k).value))

        return self.compiled.format(**self.args)

    def __packageToPath(self, package: Text):
        return path.join(*(package.split('.')))
