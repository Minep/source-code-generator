from typing import Dict, List, Text, Any


class Configurable:
    __key: Text
    __type: Text
    __value: Any
    def __init__(self, cfgDefs: Dict) -> None:
        self.__checkIfValid(cfgDefs)
        self.__key = cfgDefs["key"]
        self.__type = cfgDefs["type"].lower() 
        if not (self.__type in ["str", "int", "float"]):
            raise ValueError(
                "Invalid type for "+self.__key+": Type must be primitive! One of int, str, float")
        self.__value = None

    def __checkIfValid(self, cfgDef: Dict[Text, Any]):
        if not (("key" in cfgDef) and ("type" in cfgDef)):
            raise ValueError("Invalid configurable object!")
    
    def assignValue (self, val: Any):
        if val == None:
            return
        typeVal:Text = type(val).__name__
        if typeVal.lower() == self.__type:
            self.__value = val
        else:
            raise ValueError(
                (
                    "Could not assign value to {key}:"
                    "Type mismatch: require {needType}, but {givenType} provided"   
                ).format(
                    key=self.__key,
                    needType=self.__type,
                    givenType=typeVal
                ))
    
    @property
    def key(self):
        return self.__key

    @property
    def value(self):
        return self.__value

    @property
    def type(self):
        return self.__type

class ConfigurableCollection:
    __configurables: Dict[Text, Configurable]
    def __init__(self, configsDef: List[Dict], defaultDefs: Dict) -> None:
        self.__configurables = dict()
        for cfgDef in configsDef:
            configurable = Configurable(cfgDef)
            configurable.assignValue(defaultDefs[configurable.key])
            self.__configurables.setdefault(configurable.key, configurable)

    @property
    def configValues(self):
        return self.__configurables.values()

    def hasConfig(self, key: Text):
        return key in self.__configurables

    def getConfig(self, key: Text):
        return self.__configurables.get(key)

    def assignTo(self, key: Text, value: Any):
        if key in self.__configurables:
            self.__configurables.get(key).assignValue(value)
        else:
            raise ValueError("Could not found configuration with key: "+key)
