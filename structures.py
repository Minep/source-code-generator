from abc import ABC, abstractclassmethod, abstractmethod
from configurable import ConfigurableCollection
from typing import Any, Dict, List, Text, Union, Pattern
from pathFormatter import FormatterMananger, PathFormatter
import re

class JsonDeserializable(ABC):
    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def _checkIfValid(self, jsonObject: Dict[Text, Any]):
        pass

class Mapping(JsonDeserializable):
    fromSource: Pattern
    toPathFormatter: PathFormatter
    def __init__(
        self, 
        formatterManager: FormatterMananger, 
        object: Dict[Text, Union[Text, Dict[Text, Text]]]
    ) -> None:
        super().__init__()
        self._checkIfValid(object)
        self.fromSource = re.compile(object["map"])
        toPath = object["to"]

        if isinstance(toPath, dict):
            self.toPathFormatter = formatterManager.getFormatter(toPath["with"], toPath["format"])
        else:
            self.toPathFormatter = formatterManager.getDefaultFormatter(toPath)

    def isMatched (self, sourceName: Text) -> bool:
        return bool(self.fromSource.search(sourceName))

    def getMapped (self, sourceName: Text, configCollection: ConfigurableCollection) -> Text:
        return self.toPathFormatter.getText(configCollection, self.fromSource.match(sourceName))

    def _checkIfValid(self, jsonObject: Dict[Text, Any]):
        if not (("map" in jsonObject) and ("to" in jsonObject)):
            raise ValueError("Invalid template object!")
    


class SourceTemplate(JsonDeserializable):
    __name: Text
    __path: Text
    __configurables: ConfigurableCollection
    __mappings: List[Mapping]

    def __init__(self, formatterManager: FormatterMananger, path: Text, templateCfg: Dict) -> None:
        super().__init__()
        self._checkIfValid(templateCfg)
        self.__path = path
        self.__name = templateCfg["name"]
        self.__configurables = ConfigurableCollection(
            templateCfg["configurables"], 
            templateCfg["defaults"]
        )
        self.__mappings = []
        for mapping in templateCfg["mappings"]:
            self.__mappings.append(Mapping(formatterManager, mapping))
        
    
    def _checkIfValid(self, templateObj: Dict[Text, Any]):
        if not (("name" in templateObj) and ("configurables" in templateObj) and ("defaults" in templateObj)):
            raise ValueError("Invalid template object!")

    def getMapping(self, path: Text):
        for mapping in self.__mappings:
            if mapping.isMatched(path):
                return mapping.getMapped(path, self.__configurables)
        return path

    @property
    def name(self):
        return self.__name

    @property
    def path(self):
        return self.__path

    @property
    def configurables(self):
        return self.__configurables.configValues

    def setConfiguration(self, key: Text, val: Any):
        self.__configurables.assignTo(key, val)

    def expand(self):
        newDict = dict()
        for v in self.__configurables.configValues:
            newDict.setdefault(v.key, v.value)
        return newDict
