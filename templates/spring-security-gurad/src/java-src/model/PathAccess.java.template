package {:package:}.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public class PathAccess {
    private String path;
    private Map<String, Set<String>> roles;

    public boolean decideAccessibility (HttpMethod method, UserRoleModel roleModel) {
        if (roles.containsKey(method.toString())) {
            Set<String> intersect = new HashSet<>(roleModel.getUserRole());
            intersect.retainAll(this.roles.get(method));
            return !intersect.isEmpty();
        }
        return false;
    }

    public Set<String> getRequiredRoles (HttpMethod method) {
        return roles.getOrDefault(method.toString(), Set.of());
    }
}
