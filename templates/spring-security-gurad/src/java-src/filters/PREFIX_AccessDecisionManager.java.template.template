package {:package:}.filters;


import {:package:}.config.SecurityConfig;
import {:package:}.model.RoleBasedConfigAttribute;
import {:package:}.model.UserRoleModel;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;

import java.util.Collection;
import java.util.Iterator;

public class {:classPrefix:}AccessDecisionManager implements AccessDecisionManager {
    @Override
    public void decide(Authentication authentication, Object o, Collection<ConfigAttribute> collection) throws AccessDeniedException, InsufficientAuthenticationException {
        if(collection.isEmpty()) {
            throw new AccessDeniedException("You do not have the privilege to access this resource");
        }

        ConfigAttribute configAttribute = collection.iterator().next();
        if(!(configAttribute instanceof RoleBasedConfigAttribute)) {
            throw new AccessDeniedException("You do not have the privilege to access this resource");
        }

        RoleBasedConfigAttribute roleAttribute = (RoleBasedConfigAttribute) configAttribute;

        if (roleAttribute.hasRole(SecurityConfig.EVERYONE)) {
            return;
        }

        Object obj = authentication.getCredentials();
        UserRoleModel roleModel = (obj instanceof UserRoleModel) ? (UserRoleModel) obj : null;
        if (roleModel != null) {
            if (roleAttribute.hasRole(SecurityConfig.AURTHENTICATED) || roleAttribute.hasAnyOverlaps(roleModel)) {
                return;
            }
        }
        throw new AccessDeniedException("You do not have the privilege to access this resource");
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
