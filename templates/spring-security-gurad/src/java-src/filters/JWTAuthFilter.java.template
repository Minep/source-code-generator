package {:package:}.filters;

import {:package:}.jwt.JwtHelper;
import {:package:}.jwt.JwtContext;
import {:package:}.jwt.JwtStructure;
import {:package:}.jwt.TokenType;
import {:package:}.model.UserRoleModel;
import {:package:}.proxy.UserModelResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

public class JWTAuthFilter extends OncePerRequestFilter {

    @Autowired
    private JwtHelper helper;

    @Autowired
    private UserModelResolver userModelResolver;

    private String refreshRequestUrl;

    public JWTAuthFilter(String refreshRequestUrl) {
        this.refreshRequestUrl = refreshRequestUrl;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String authHeader = request.getHeader(JwtHelper.AUTH_START);
        if(authHeader!=null && authHeader.startsWith(JwtHelper.TOKEN_START)) {
            Optional<JwtContext> jwtCtx = helper.getContext(authHeader);
            jwtCtx.ifPresent(jwtContext -> processJwt(jwtContext, request));
        }
        chain.doFilter(request, response);
    }

    private void processJwt(JwtContext jwtContext, HttpServletRequest request) throws UsernameNotFoundException {
        JwtStructure structure = jwtContext.getStructure();
        TokenType tokenType = structure.getIssuedTokenType();
        if(tokenType != TokenType.INVALID || !jwtContext.validate()) {
            return;
        }

        String uid = structure.getIssuedSubject();
        UserRoleModel userModel = userModelResolver.resolveUserModel(uid);
        if(userModel == null || !structure.getTokenId().equals(userModel.getUserHash())){
            return;
        }

        AntPathMatcher matcher = new AntPathMatcher();
        boolean matchResult = matcher.match(refreshRequestUrl, request.getRequestURI());

        if (tokenType == TokenType.ACCESS && matchResult) {
            return;
        }
        if (tokenType == TokenType.REFRESH && !matchResult) {
            return;
        }

        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(jwtContext, userModel, Collections.emptyList());
        token.setDetails(new WebAuthenticationDetails(request));
        SecurityContextHolder.getContext().setAuthentication(token);
    }
}
