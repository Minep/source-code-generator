package {:package:}.config;

import {:package:}.model.PathAccess;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

@Setter
@Getter
@Validated
@ConfigurationProperties("security")
public class SecurityConfig {
    public static final String EVERYONE = "ALL";
    public static final String AURTHENTICATED = "AUTHED";

    @NotEmpty
    private Map<String, Integer> systemRoles;
    @NotBlank
    private String jwtSigningKey;
    @NotBlank
    private String refreshPath;
    @NotNull
    private List<PathAccess> accessControl;

    public Map<String, List<String>> getPublicAccessable() {
        Map<String, List<String>> result = new HashMap<>();
        accessControl.forEach(path -> {
            for (Map.Entry<String, Set<String>> entry : path.getRoles().entrySet()) {
                if (entry.getValue().contains(EVERYONE)) {
                    result.compute(path.getPath(), (key, value) -> {
                        if (value == null) {
                            return Arrays.asList(entry.getKey());
                        }
                        value.add(entry.getKey());
                        return value;
                    });
                }
            }
        });
        return result;
    }
}
