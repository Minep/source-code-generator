from pathlib import Path
from prompt_toolkit.validation import ValidationError, Validator


class FloatValidator(Validator):
    def validate(self, document):
        text = document.text

        try:
            float(text)
        except:
            raise ValidationError(message='This input is not a valid floating point number')

class IntValidator(Validator):
    def validate(self, document):
        text = document.text

        try:
            int(text)
        except:
            raise ValidationError(message='This input is not a valid integer')

class PathValidator(Validator):
    def validate(self, document):
        text = document.text

        if text:
            path = Path(text)
            if not path.exists():
                raise ValidationError(message='This path is not exist.')    
