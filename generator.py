from typing import Text
from jinja2.environment import Template
from jinja2.loaders import BaseLoader, FileSystemLoader
from structures import SourceTemplate
from jinja2 import Environment
import os
from pathlib import Path


class Generator:
    __template: SourceTemplate
    __env: Environment
    def __init__(self, env: Environment, template: SourceTemplate) -> None:
        self.__template = template
        self.__env = env
        # override loader
        self.__env.loader = FileSystemLoader(template.path)

    def run(self, outBase: Text, outName: Text):
        outDir = Path(outBase).joinpath(outName)
        outDir.mkdir(exist_ok=True)
        configurablesVars = self.__template.expand()
        templateBasePath = self.__template.path

        for dir, _, files in os.walk(templateBasePath):
            templateRelDir = os.path.relpath(dir, templateBasePath)
            relDir = self.__template.getMapping(templateRelDir)
            path = outDir.joinpath(relDir)
            if not path.exists() or not path.is_dir():
                path.mkdir(parents=True, exist_ok=True)
            for file in files:
                outPath = self.__template.getMapping(str(path.joinpath(file)))
                template = self.__env.get_template(os.path.join(templateRelDir, file))
                result = template.render(**configurablesVars)
                with open(outPath, "w+") as f:
                    f.write(result)
