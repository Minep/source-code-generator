def parseStringFormat(string:str):
    args = dict()
    state=0
    currentInx = 0
    for i in range(len(string)):
        v = string[i]
        if state == 0 and v == '{':
            currentInx = i + 1
            state = 1
        elif state == 1 and v == '}':
            arg = string[currentInx : i]
            if len(arg) > 0:
                args.setdefault(arg, "")
            currentInx = i + 1
            state = 0
    return args