from abc import ABC, abstractmethod
from formatters import JavaPackageFormatter, PathFormatter, RegexFormatter
from configurable import ConfigurableCollection
from typing import Any, Callable, Dict, Match, Text


class TrivialFormatter(PathFormatter):
    def __init__(self, path: Text) -> None:
        super().__init__(path)
    
    def getText(self, configCollection: ConfigurableCollection, processedMatch: Match) -> Text:
        return super().getText(configCollection, processedMatch)


class FormatterMananger:
    __registry: Dict[Text, Callable[[Text], PathFormatter]]
    def __init__(self) -> None:
        self.__registry = {
            "default": lambda path: TrivialFormatter(path),
            "java-package-path": lambda path: JavaPackageFormatter(path),
            "regex": lambda path: RegexFormatter(path),
        }
        #todo
    
    def getFormatter(self, name: Text, text: Text):
        className = self.__registry.get(name)
        if className == None:
            return self.getDefaultFormatter(text)
        return className(text)
    
    def getDefaultFormatter(self, text: Text):
        return TrivialFormatter(text)
